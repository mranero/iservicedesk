/* add-on script */
var JWT_TOKEN;

$(document).ready(function () {
    JWT_TOKEN = $('#token').attr('content');

    function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    var projectKey = getParameterByName("projectKey");

    $(document.body).on('click', "#store-changes", function (event) {
        console.log("Project key:");
        var app1 = $('#app1').val();
        var app2 = $('#app2').val();
        $.ajax({
            type: 'POST',
            url: 'update-baseline/' + projectKey,
            contentType: 'application/json',
            data: JSON.stringify({ value: {app1, app2 }}),
        })
            .done(() => {
                var message = AP.messages.info('Done', 'La Linea Base se ha actualizado correctamente');
                setTimeout(function () {
                    AP.messages.clear(message);
                }, 2000);
            })
            .fail(() => {
                AP.messages.error('Error', 'Se ha producido un error al almacenar la Linea Base');
            });
        return false;
    });
    $(document.body).on('click', "#initial", function (event) {
        var app1 = $('#app1').val();
        var app2 = $('#app2').val();
        $.ajax({
            type: 'POST',
            url: 'update-baseline/' + projectKey,
            contentType: 'application/json',
            data: JSON.stringify({ value: { app1, app2 } }),
        })
            .done(() => {
                var message = AP.messages.info('Done', 'La Linea Base se ha iniciado correctamente');
                setTimeout(function () {
                    AP.messages.clear(message);
                }, 2000);
            })
            .fail(() => {
                AP.messages.error('Error', 'Se ha producido un error al iniciar la Linea Base');
            });
        return false;
    });
    $.ajaxSetup({
        headers: { 'Authorization': "JWT " + JWT_TOKEN }
    });

    $(document).ajaxSuccess(function (event, xhr, settings) {
        refreshToken(xhr);
    });

    function refreshToken(jqXHR) {
        JWT_TOKEN = jqXHR.getResponseHeader('X-acpt');
    }
});