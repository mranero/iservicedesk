$(function () {
    AP.require(["events"], function (events) {
        var requestProperties = new AP.jiraServiceDesk.RequestProperties();

        events.on("jira-servicedesk-request-properties-serialize", function () {
            requestProperties.serialize({
                iCMDB_active_issue_property: $("#idactive").val()
            });
        });

        events.on("jira-servicedesk-request-properties-validate", function () {
            var valid = true;
            //$("#f").append("<p class='field-error'>" + $("#priority").val()+"</p>");
            requestProperties.validate(valid)
        });
    });
});

