AP.require(["jira"], function (jira) {
    jira.WorkflowConfiguration.onSave(function () {
        // Display a nice green flag using the Flags JavaScript API.
        var flag = AP.flag.create({
            title: 'iNortegas.',
            body: 'Incluida post-funci�n iNortegas',
            type: 'success',
            actions: {
                'actionkey': 'Click'
            }
        });
        console.log(arguments);
        return "";
    });
    jira.WorkflowConfiguration.onSaveValidation(function () {

        return true;
    });
});
