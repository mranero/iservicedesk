var request = require('request');
var moment = require('moment');
var fileUtils = require('./fileUtils.js');
var fs = require('fs');

/**
 * Utilities module to interact with Jira and Confluence API REST
 * @module utils
 * @example
 * var utils = require('../utils_JC/utils.js');
 */
module.exports = {

    /**
     * Build options object for Node.js Request module used to create Confluence pages
     * @param {string} urlConfluence - The Confluence API REST domain url
     * @param {string} auth - jwt token
     * @param {object} bodyData - body object for Node.js Request module
     * @returns {string} - json string that represent options object for request module
     * @example
     * var options = utils.crearOpciones(urlConfluence, auth, bodyData);
     */
    crearOpciones: function (urlConfluence, auth, bodyData) {
        var opciones = {
            url: urlConfluence + '/rest/api/content',
            method: 'POST',
            headers: {
                'Authorization': auth,
                //'X-Atlassian-Token': 'nocheck',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: bodyData
        };
        return opciones;
    },

    /**
     * Build options object for Node.js Request module used to get Confluence pages data
     * @param {string} urlConfluence - The Confluence API REST url
     * @param {string} auth - jwt token
     * @param {string} titulo - Page title
     * @param {string} espacio - Confluence space
     * @returns {string} - json string that represent options object for request module
     * @example
     * var options = utils.crearOpcionesGET(urlConfluence, auth, titulo, espacio);
     */
    crearOpcionesGET: function (urlConfluence, auth, titulo, espacio) {
        var encoded = encodeURI(urlConfluence + '/rest/api/content?title=' + titulo + '&spaceKey=' + espacio);
        var opciones = {
            url: encoded,
            method: 'GET',
            headers: {
                'Authorization': auth,
                //'X-Atlassian-Token': 'nocheck',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };

        return opciones;
    },
    /**
     * Build options object for Node.js Request module used to get Confluence pages data
     * @param {string} urlConfluence - The Confluence API REST domain url
     * @param {string} auth - jwt token
     * @param {string} iD - Page ID
     * @param {string} iDAtt - Attachment ID
     * @param {object} bodyData - body object for Node.js Request module
     * @returns {string} - json string that represent options object for request module
     * @example
     * var options = utils.crearOpcionesLabel(urlConfluence, auth, iD, iDAtt, bodyData);
     */
    crearOpcionesLabel: function (urlConfluence, auth, iD, iDAtt, bodyData) {
        var encoded = encodeURI(urlConfluence + '/rest/api/content/' + iD + '/child/attachment/' + iDAtt);
        var opciones = {
            url: encoded,
            method: 'PUT',
            headers: {
                'Authorization': auth,
                //'X-Atlassian-Token': 'nocheck',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: bodyData
        };

        return opciones;
    },
    /**
     * Build array of Confluence pages labels
     * @param {object[]} etiquetas - Array of Confluence page labels
     * @returns {string} - json array string that represent the input labels
     * @example
     * var labels = utils.crearLabels(etiquetas);
     */
    crearLabels: function (etiquetas) {
        labels = `[`;
        for (var i = 0; i < etiquetas.length; i++) {
            labels += `{
                "prefix": "global",
                "name": "${etiquetas[i]}"
            }`
            if ((i + 1) < etiquetas.length) labels += `,`;
        }
        labels += ` ]`;
        return labels;
    },

    /**
     * Build options object for Node.js Request module used to create Confluence parent pages data
     * @param {string} titulo - Page title
     * @param {string} espacio - Confluence space
     * @param {string} value - HTML that represent a Confluence macro
     * @returns {string} - json array string that represent options object
     * @example
     * var bodyData = utils.crearBody(aplicacionPaginaApp, espacio, value);
     */
    crearBody: function (titulo, espacio, value) {
        var body = `{
			"title":"${titulo}",
			"type":"page", 
			"space":{
				"key":"${espacio}"
			},
			"body":{
				"storage":{
							"value":"<p>${value}</p>",
							"representation":"storage"
				}
			}
		}`;

        return body;
    },

    /**
     * Build options object for Node.js Request module used to create Confluence child pages data
     * @param {string} titulo - Page title
     * @param {string} espacio - Confluence space
     * @param {string} ancestor - Parent Confluence page
     * @param {string} value - HTML that represent a Confluence macro
     * @param {object[]} labels - Array of Confluence page labels
     * @returns {string} - json array string that represent options object
     * @example
     * bodyData = utils.crearBodyHijo(titulo, espacio, idAncestor, value, labels);
     */
    crearBodyHijo: function (titulo, espacio, ancestors, value, labels) {
        var body = `{
			"title":"${titulo}",
			"type":"page", 
			"space":{
				"key":"${espacio}"
			},
			"ancestors":[
				{
					"id":"${ancestors}"
				}
			],
			"body":{
				"storage":{
						"value":"<p>${value}</p>",
						"representation":"storage"
				}
			},
            "metadata":{
                "labels": ${labels}
            }
        }`;

        return body;
    },
     /**
     * Build options object for Node.js Request module used to create Confluence child pages data
     * @param {string} id - Attachment ID
     * @param {object[]} labels - Array of Confluence page labels
     * @returns {string} - json array string that represent options object
     * @example
     * bodyData = utils.crearBodyLabels(id, labels);
     */
    crearBodyLabels: function (id, labels) {
        var body = `{
            "id":"${id}",
            "version":{
              "number": 1
            },
            "type": "page",            
            "metadata":{
                "labels": ${labels}
            }
        }`;

        return body;
    },
    /**
     * HTTP request client 'request' with Promise
     * @param {object} options - options object Request
     * @return {Promise<string>} The data from the URL
     * @example
     * var body = await utils.requestPROMISE(options).catch(error => fileUtils.comentar('ERROR: ' + error));
     */
    requestPROMISE: function (options) {
        return new Promise((resolve, reject) => {
            request(options, (error, response, body) => {
                if (error) reject(new Error(error));
                resolve(body);
            });
        });
    },
    requestPROMISEResponse: function (options) {
        return new Promise((resolve, reject) => {
            request(options, (error, response, body) => {
                if (error) reject(new Error(error));
                resolve(response);
            });
        });
    },
    /**
     * HTTP request async to get Confluence page ID
     * @param {object} options - options object Request
     * @return {string} Page ID
     * @example
     * await utils.getIDPagina(options).then(iD => idPageEstado = iD).catch(error => fileUtils.comentar('::ERROR: CrearLabels::' + error));
     */
    getIDPagina: async function (options) {
        try {
            var body = await module.exports.requestPROMISE(options).catch(error => fileUtils.comentar('ERROR: ' + error));
            var newBody = JSON.parse(body.toString());
            var iD = newBody.id;
            return iD;

        } catch (error) {
            console.error('::ERROR: getIDPagina::');
            console.error(error);
        }

    },

    /**
     * HTTP request async to get Confluence page title
     * @param {object} options - options object Request
     * @return {string} Page title
     * @example
     * await utils.getIDPaginaPorTitutlo(options).then(iD => idPage = iD).catch(error => fileUtils.comentar('ERROR: ' + error));
     */
    getIDPaginaPorTitutlo: async function (options) {
        try {
           // fileUtils.comentar('/******** iConfluence$$$$$$$$$' + JSON.stringify(options));
            var body = await module.exports.requestPROMISE(options).catch(error => fileUtils.comentar('ERROR: ' + error));   
            //fileUtils.comentar('/******** iConfluence#########' + body.toString());
            var newBody = JSON.parse(body.toString());
            var iD
            if (newBody.results.length != 0) {
                iD = Object.values(newBody.results[0])[0].toString();
            } else {
                iD = -1
            }    
            return iD;

        } catch (error) {
            console.error('::ERROR: getIDPaginaPorTitulo::');
            console.error(error);
        }
    },

    /**
     * Save issue properties. It&acute;s used to store SLA values associated with issues
     * @param {string} urlJira - The Jira API REST domain url
     * @param {string} issuekey - Jira issue key
     * @param {string} auth - jwt token
     * @param {string} bsi - json issue property content
     * @param {string} sla - Issue property ID
     * @example
     * utils.put_SLA_into_entity(urlJira, issuekey, auth, bsi1, sla);
     */

    getTokenRemedy: async function (urlLogin, username, password) {

        var options = {
            method: 'POST',
            url: urlLogin,
            headers:
            {
                'cache-control': 'no-cache',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            form:
            {
                username: username,
                password: password
            }
        };
        var body = await module.exports.requestPROMISE(options).catch(error => fileUtils.comentar('ERROR: ' + error));
        return body;
    },
    createTicketRemedy: async function (urlCreate, token, bodyCreate) {
        var options = {
            method: 'POST',
            url: urlCreate,
            headers:
            {
                'cache-control': 'no-cache',
                'Content-Type': 'application/json',
                'Authorization': 'AR-JWT ' + token,
            },
            body: bodyCreate
        };
        var res = await module.exports.requestPROMISEResponse(options).catch(error => fileUtils.comentar('ERROR: ' + error));
        return res;
    },
    createIncidentRemedy: async function (urlCreate, token) {
        var options = {
            method: 'GET',
            url: urlCreate,
            headers:
            {
                'cache-control': 'no-cache',
                'Content-Type': 'application/json',
                'Authorization': 'AR-JWT ' + token,
            }
        };
        var body = await module.exports.requestPROMISE(options).catch(error => fileUtils.comentar('ERROR: ' + error));
        return body;
    },
    editIssue: async function (urlJira, auth, issuekey, incident, status) {
        var bodyData = '';
        if (status == '10059' || status == '10022' ) {
            bodyData = `{
                          "fields": {
                            "customfield_10227": ${incident}
                           }
		                }`;
        } else {
            bodyData = `{
                          "fields": {
                            "customfield_10232": ${incident}
                           }
		                }`;
        }
        var options = {
            method: 'PUT',
            url: urlJira + '/rest/api/3/issue/' + issuekey,
            headers: {
                'Authorization': auth,
                'cache-control': 'no-cache',
                'Content-Type': 'application/json'
            },
            body: bodyData
        };

        var body = await module.exports.requestPROMISEResponse(options).catch(error => fileUtils.comentar('ERROR: ' + error));
        return body;
    },
    editIssueBudget: async function (urlJira, auth, issuekey, budget) {
        var bodyData = '';
            bodyData = `{
                          "fields": {
                            "customfield_10243": ${budget}
                           }
		                }`;
        fileUtils.comentar('/******** iBudget$$$$$$$$$' +bodyData);
        var options = {
            method: 'PUT',
            url: urlJira + '/rest/api/3/issue/' + issuekey,
            headers: {
                'Authorization': auth,
                'cache-control': 'no-cache',
                'Content-Type': 'application/json'
            },
            body: bodyData
        };
        var body = await module.exports.requestPROMISEResponse(options).catch(error => fileUtils.comentar('ERROR: ' + error));
        return body;
    },
    put_SLA_into_entity: function (urlJira, issuekey, auth, bsi, sla) {

        var options = {
            method: 'PUT',
            url: urlJira + '/rest/api/3/issue/' + issuekey + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            },
            body: bsi
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(
                'Response: ' + response.statusCode + ' ' + response.statusMessage
            );
        });
        return '';
    },
    /**
     * Save issue properties. It&acute;s used to store SLA values associated with issues
     * @param {Date} createdDate - SLA created date
     * @param {Date} dfDate - SLA target date
     * @param {Date} closeTime - SLA finalized date
     * @param {boolean} paused - 0: SLA is not paused; 1:SLA is paused
     * @param {boolean} breached - 0: SLA Time is not between createdDate and dfdate; 1:SLA Time has reached dfDate
     * @param {boolean} withinCalendarHours - 0: SLA is not fulfilled; 1:0: SLA is fulfilled
     * @param {boolean} withinWCalendarHours - 0: SLA Time is not out working hours (according to the file workCalendar.json); 1: SLA Time is out working hours (according to the file workCalendar.json)
     * @param {boolean} closed - 0: SLA is not closed; 1:SLA is closed
     * @return {string} json string that represent issue SLA properties
     * @example
     * var bsi1 = module.exports.build_SLA_Information(startDate, fDate, 0, 0, 0, 0, utils.inWorkingHours(), 0);
     */
    addWeekdays: function (date, days) {
        date = moment(date); // use a clone
        while (days > 0) {
            date = date.add(1, 'days');
            // decrease "days" only if it's a weekday.
            if (date.isoWeekday() !== 6 && date.isoWeekday() !== 7) {
                days -= 1;
            }
        }
        return date;
    },
    getBusinessDays: function (startDate, endDate){
        var startDateMoment = moment(startDate);
        var endDateMoment = moment(endDate)
        var days = Math.round(startDateMoment.diff(endDateMoment, 'days') - startDateMoment.diff(endDateMoment, 'days') / 7 * 2);
        if (endDateMoment.day() === 6) {
            days--;
        }
        if (startDateMoment.day() === 7) {
            days--;
        }
        return days;
    },
    build_SLA_Information: function (createdDate, dfDate, closeTime, paused, breached, withinCalendarHours, withinWCalendarHours, closed) {
        //var fcreatedDate = moment(createdDate).format("ll");
        var fcreatedDate = moment(createdDate).format();
        var jiradueDate = moment(dfDate).format();
        var dfDatet = moment(dfDate).format("ll");

        var a = moment(createdDate);
        var b = moment(jiradueDate);
        var daysfgol = a.to(b, true);
        fileUtils.comentar('CLOSE TIME - BUILD SLA: ==> ' + closeTime);
        //if (closeTime = 0) closeTime = "";

        var elapsed = moment(createdDate).toNow(true);
        var remaining = moment(jiradueDate).fromNow(); 
        //var remaining = Math.abs(module.exports.getBusinessDays(moment().format(), jiradueDate));
        //remaining++;
        var body = `{
                                "startTime": {
                                    "jira": "${createdDate}",
                                    "friendly": "${fcreatedDate}"
                                },
                                "breachTime": {
                                    "jira": "${jiradueDate}",
                                    "friendly": "${dfDatet}"
                                },
                                "closeTime": {
                                    "jira": "${closeTime}"
                                },
                                "breached": ${breached},
                                "paused": ${paused},
                                "withinCalendarHours": ${withinCalendarHours},
                                "withinWCalendarHours": ${withinWCalendarHours},   
                                "closed": ${closed},
                                "goalDuration": {
                                    "friendly": "${daysfgol}"
                                },
                                "elapsedTime": {
                                    "friendly": "${elapsed}"
                                },
                                "remainingTime": {
                                    "friendly": "${remaining}"
                                }
                    }`
        return body;
    },
    build_SLA_Reject: function (data) {
        var j = JSON.parse(data);

        var numRejects = 1;
        if (j.value!=undefined) {
            numRejects = j.value.numRejects;
            numRejects++;
        }
        var body = `{
                                "numRejects":  ${numRejects}
                    }`
        return body;
    },
    /**
     * Test if actual time is in working hours (according to the file workCalendar.json)
     * @return {boolean} 
     * @example
     * var Iswh = utils.inWorkingHours();
     */
    inWorkingHours: function () {
        var result;
        var wcalendar = JSON.parse(fs.readFileSync('./workCalendar.json', 'utf8'));
        var todayIs = moment().format("d");
        var hourstartcalendarIs = moment(wcalendar.week[todayIs].start, "HH:mm");
        var hourendcalendarIs = moment(wcalendar.week[todayIs].end, "HH:mm");
        if (moment(moment(), "HH:mm").isBetween(hourstartcalendarIs, hourendcalendarIs, null, '[]')) {
            result = 1;
        }
        else {
            result = 0;
        }
        return result;
    },

    /**
     * Get issue properties. It&acute;s used to get SLA values associated with issues
     * @param {string} urlJira - The Jira API REST domain url
     * @param {string} issuekey - Jira issue key
     * @param {string} auth - jwt token
     * @param {string} sla - Issue property ID
     * @return {string} json string that represent issue SLA properties
     * @example
     * utils.get_SLA_entity(urlJira, issuekey, auth, sla);
     */
    get_SLA_entity: function (urlJira, issuekey, auth, sla) {

        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/issue/' + issuekey + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(
                'Response: ' + response.statusCode + ' ' + response.statusMessage
            );
            return (JSON.parse(body));
        });
    },
    get_project_entity: function (urlJira, project, auth, sla) {

        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/project/' + project + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(
                'Response: ' + response.statusCode + ' ' + response.statusMessage
            );
            return (JSON.parse(body));
        });
    },
    /**
     * Start SLA for an issue
     * @param {string} urlJira - The Jira API REST domain url
     * @param {string} issuekey - Jira issue key
     * @param {string} auth - jwt token
     * @param {string} sla - Issue property ID
     * @param {Date} updatedDate - SLA update date
     * @param {Date} dfDate - SLA target date
     * @example
     * // Approve planning. Start DF
     * utils.start_SLA(urlJira, issuekey, auth, 'slaDF', updatedDate, dfDate);
     */
    start_SLA: function (urlJira, issuekey, auth, sla, startDate, fDate) {
        fileUtils.comentar('/******** START SLA ==> ' + startDate);
        var bsi1 = module.exports.build_SLA_Information(startDate, fDate, startDate, 0, 0, 0, module.exports.inWorkingHours(), 0);
        module.exports.put_SLA_into_entity(urlJira, issuekey, auth, bsi1, sla);
        return '';
    },

    /**
 * Pause SLA for an issue
 * @param {string} urlJira - The Jira API REST domain url
 * @param {string} issuekey - Jira issue key
 * @param {string} auth - jwt token
 * @param {string} sla - Issue property ID
 * @param {Date} updatedDate - SLA update date
 * @param {Date} dfDate - SLA target date
 * @example
 * utils.pause_SLA(urlJira, issuekey, auth, 'slaDT', updatedDate, dfDate);
 */
    pause_SLA: function (urlJira, issuekey, auth, sla, updatedDate, fDate) {
        fileUtils.comentar('/******** PAUSE SLA ==> ');
        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/issue/' + issuekey + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var j = JSON.parse(body);
            var bsi2 = module.exports.build_SLA_Information(j.value.startTime.jira, fDate, updatedDate, 1, 0, 0, module.exports.inWorkingHours(), 0);
            module.exports.put_SLA_into_entity(urlJira, issuekey, auth, bsi2, sla);
        });
        return '';
    },
    /**
     * End SLA for an issue
     * @param {string} urlJira - The Jira API REST domain url
     * @param {string} issuekey - Jira issue key
     * @param {string} auth - jwt token
     * @param {string} sla - Issue property ID
     * @param {Date} updatedDate - SLA update date
     * @param {Date} dfDate - SLA target date
     * @example
     * // Approve planning. Start DF
     * utils.end_SLA(urlJira, issuekey, auth, 'slaIMP', updatedDate, dfDate);
     */
    end_SLA: function (urlJira, issuekey, auth, sla, updatedDate, fDate) {
        fileUtils.comentar('/******** END SLA ==> ' + updatedDate);
        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/issue/' + issuekey + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var j = JSON.parse(body);
            fileUtils.comentar('/******** CLOSE TIME - END SLA: ==> ' + updatedDate);
            var bsi2 = module.exports.build_SLA_Information(j.value.startTime.jira, fDate, updatedDate, 1, 1, 0, module.exports.inWorkingHours(), 1);
            module.exports.put_SLA_into_entity(urlJira, issuekey, auth, bsi2, sla);
        });
        return '';
    },

    /**
     * Resume SLA for an issue
     * @param {string} urlJira - The Jira API REST domain url
     * @param {string} issuekey - Jira issue key
     * @param {string} auth - jwt token
     * @param {string} sla - Issue property ID
     * @param {Date} updatedDate - SLA update date
     * @param {Date} dfDate - SLA target date
     * @example
     * utils.resume_SLA(urlJira, issuekey, auth, 'slaDF', updatedDate, dfDate);
     */
    resume_SLA: function (urlJira, issuekey, auth, sla, updatedDate, fDate) {
        fileUtils.comentar('/******** RESUME SLA ==> ');
        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/issue/' + issuekey + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var j = JSON.parse(body);
            var bsi2 = module.exports.build_SLA_Information(j.value.startTime.jira, fDate, updatedDate, 0, 0, 0, module.exports.inWorkingHours(), 0);
            module.exports.put_SLA_into_entity(urlJira, issuekey, auth, bsi2, sla);
        });
        return '';
    },
    /**
     * reject SLA for an issue
     * @param {string} urlJira - The Jira API REST domain url
     * @param {string} issuekey - Jira issue key
     * @param {string} auth - jwt token
     * @param {string} sla - Issue property ID
     * @param {Date} updatedDate - SLA update date
     * @param {Date} dfDate - SLA target date
     * @example
     * // Approve planning. Start DF
     * utils.reject_SLA(urlJira, issuekey, auth, 'slaIMP', updatedDate, dfDate);
     */
    reject_SLA: function (urlJira, issuekey, auth, sla, updatedDate, fDate) {
        fileUtils.comentar('/******** REJECT SLA ==> ' + updatedDate);
        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/issue/' + issuekey + '/properties/' + sla,
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var j = JSON.parse(body);
            fileUtils.comentar('/******** REJECT SLA: ==> ' + updatedDate);
            var bsi2 = module.exports.build_SLA_Reject(body);
            module.exports.put_SLA_into_entity(urlJira, issuekey, auth, bsi2, sla);
        });
        return '';
    }
};