﻿/** In "./routes/index.js" is the router. Routing refers to determining how yhe application responds to client request to a particular endpoint, which is a URI (or path) and a specific HTTP request method (GET, POST, and so on).
 * Each route can have one or more handler functions, which are executed when the route is matched.
 * @module routes/index
 */
module.exports = function (app, addon) {

    //************************************ REQUIRES **********************************//
    var request = require('request');
    var BL = require("./BL")();
    /** Parse, validate, manipulate, and display dates and times in JavaScript {@link https://momentjs.com/}
     * @module moment/times
     * @requires moment
     */
    /**
     * moment module
     * @var
     */
    var moment = require('moment');
    var fs = require('fs');
    /** Node.js module which wraps Atlassian's Confluence API {@link https://www.npmjs.com/package/confluence-api}
     * @module confluence/api
     * @requires confluence-api
     * @example
     * var config = {
            username: user,
            password: pass,
            baseUrl: urlConfluence,
        };
     * var confluence = new Confluence(config);
     */
    /**
     * confluence-api module
     * @var
     */
    var Confluence = require('confluence-api');
    var jwt = require('jsonwebtoken');

    var fileUtils = require('../utils_JC/fileUtils.js');
    var utils = require('../utils_JC/utils.js');

    //************************************ PARAMETROS DE CONFIGURACI�N ************************//
    /** JSON with environment information (usr, jira instance,..). See "configPlugin.json" file.
     *  @memberof module:express/router~routes
     *  @var {Object} */
    var param = JSON.parse(fs.readFileSync('./configPlugin.json', 'utf8'));
    /** JSON with working hours information. See "workCalendar.json" file
     *  @memberof module:express/router~app
     *  @var {Object} */
    var wcalendar = JSON.parse(fs.readFileSync('./workCalendar.json', 'utf8'));
    /** jwt authentication token. For more information see the url: {@link https://jwt.io/introduction/}
     *  @memberof module:express/router~app
     *  @var {string} */
    var user = param.usuarios[0].user;
    var pass = param.usuarios[0].pass;
    var auth = 'Basic ' + Buffer.from(user + ':' + pass).toString('base64');
    /** Jira instance url
     *  @memberof module:express/router~app
     *  @var {string} */
    var urlJira = param.usuarios[0].urlJira;
    /** Confluence instance url 
     *  @memberof module:express/router~app
     *  @var {string} */
    var urlConfluence = urlJira + '/wiki';
    /** Confluence Space ID 
     *  @memberof module:express/router~app
     *  @var {string} */
    var espacio = param.usuarios[0].espacio;
    /** Temporal folder to store docs 
     *  @memberof module:express/router~app
     *  @var {string} */
    var carpetaDocs = param.usuarios[0].carpetaDocs;
    var config = {
        username: user,
        password: pass,
        baseUrl: urlConfluence,
    };
    var confluence = new Confluence(config);

    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });

    /**
     * Route serving link to allow access to documents in portal. The documents are stored in Confluence
     * @name get/sd-portal-request-view-content
     * @async
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/sd-portal-request-view-content', addon.authenticate(), async (req, res) => {
        try {
            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/2/issue/' + req.param('issueKey'),
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };
            request(options, async function (error, response, body) {
                if (error) throw new Error(error);
                let json = JSON.parse(body);
                let issuePage = json.key + ': ' + json.fields.summary;
                let idPage;
                options = utils.crearOpcionesGET(urlConfluence, auth, issuePage, espacio);

                await utils.getIDPaginaPorTitutlo(options).then(iD => idPage = iD).catch(error => fileUtils.comentar('ERROR: ' + error));

                idPage = urlConfluence + '/spaces/' + espacio + '/pages/' + idPage;
                try {
                    res.render('docs', {
                        title: 'Documentos asociados',
                        docs: idPage
                    });
                } catch (error) {
                    res.render('docs_error', {
                    });
                }
            });
        } catch (error) {
            res.render('docs_error', {
            });
        }
    });

    /**
     * Route serving link to store jira issue attachments in Confluence. It's triggered through postfunction workflows.
     * @name post/confluence-integration
     * @async
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.post('/confluence-integration', addon.authenticate(), async (req, res) => {
        try {

            /*************************** ESTABLECER VARIABLES INICIALES */

            const issueJSON = req.body;
            //fileUtils.comentar("/******** iConfluence::JSON body==> " + JSON.stringify(issueJSON));
            const issuekey = issueJSON.issue.key;
            const issuesummary = issueJSON.issue.fields.summary;
            const issuedesc = issueJSON.issue.fields.description;
            const nEstado = issueJSON.issue.fields.status.id;

            //Forma de coger elementos cuando devuelve un OBJECT (hay que transformarlo a STRING)
            //const aplicacion = Object.values(issueJSON.issue.fields.customfield_10201)[1];
            const componenteJira = issueJSON.issue.fields.customfield_10201.value; //ID CAMPO EN TEST
            //const componenteJira = issueJSON.issue.fields.customfield_10162.value; //ID CAMPO  APLICACION EN PRODUCCION
            let issuePage = issuekey + ': ' + issuesummary;
            let idAncestor11 = 0;
            let idAncestor1 = 0;
            let idAncestor2 = 0;
            let idAncestor3 = 0;
            let first = 0;
            let iniciales = '';
            let iniciales_mod = '';
            const pGE = 'Evolutivos';
            var titulo = '';
            var aplicacion = '';
            var grupo = '';
            var modulo = '';
            var componente = '';
            fileUtils.comentar("/******** iConfluence::JIRA issue ==> " + issuekey);
            var param = JSON.parse(fs.readFileSync('./configPlugin.json', 'utf8'));
            //var usuarioC = issueJSON.issue.fields.creator.name;
            var usuarioC = issueJSON.user.name;
            fileUtils.comentar("/******** iConfluence::JIRA user name ==> " + usuarioC);
            var i = 0;
            var userC = user;
            var passC = pass;
            while (i < param.usuarios.length) {
                if (param.usuarios[i].name == usuarioC) {
                    userC = param.usuarios[i].user;
                    passC = param.usuarios[i].pass;
                }
                i++;
            }
            fileUtils.comentar("/******** iConfluence::JIRA user ==> " + userC);
            fileUtils.comentar("/******** iConfluence::JIRA api key ==> " + passC);
            var authC = 'Basic ' + Buffer.from(userC + ':' + passC).toString('base64');

            //Array que almacena temporalmente los documentos adjuntos del ticket
            var attachArray = []; 
            /***********************************************************************************************
             ***********************************  FUNCIONES **********************************************/

            function unhyphenate(str) {
                return str.replace(/(\w)(-)(\w)/g, '$1 $3');
            }

            function string_to_array(str) {
                return str.trim().split(" ");
            };

            //FUNCION QUE MAPEA LAS FASES EN FUNCION DEL ESTADO DEL FLUJO
            function fase(E) {

                fileUtils.comentar("/******** iConfluence::Estado ==> " + E);
                switch (E) {
                    case '10053':
                        // code block
                        return "Requerimientos"
                        break;
                    case '10090':
                        // code block
                        return "Requerimientos"
                        break;
                    case '10036':
                        // code block
                        return 'Estimaci\xF3n';
                        break;
                    case '10037':
                        // code block
                        return 'Planificaci\xF3n';
                        break;
                    case '10057':
                        // code block
                        return 'Dise\xF1o Funcional';
                        break;
                    case '10039':
                        // code block
                        return 'Dise\xF1o T\xE9cnico';
                        break;
                    case '10059':
                        // code block
                        return 'Transporte Integraci\xF3n';
                        break;
                    case '10022':
                        // code block
                        return 'Transporte Integraci\xF3n';
                        break;
                    case '10062':
                        // code block
                        return 'Pruebas Proveedor';
                        break;
                    case '10063':
                        // code block
                        return 'Pruebas TI';
                        break;
                    case '10064':
                        // code block
                        return 'Pruebas Negocio';
                        break;
                    case '10094':
                        // code block
                        return 'Transporte Producci\xF3n';
                        break;
                    default:
                        // code block
                        fileUtils.comentar("/******** iConfluence::NO ENCUENTRA ESTADO");
                        return '';
                }

            }

            //FUNCION QUE CREA LOS ATTACHMENT
            function crearAttachment(espacio, idPageEstado, rutaF) {
                return new Promise((resolve, reject) => {
                    confluence.createAttachment(espacio, idPageEstado, rutaF, function (error, data) {
                        //update attachment properties API
                        if (error) reject(new Error(error));
                        resolve(data);
                    })
                });
            };

            //FUNCION QUE PONE LOS LABEL EN LOS ATTACHMENT
            function crearLabelAttachment(options) {
                return new Promise((resolve, reject) => {
                    request(options, (error, response, body) => {
                        if (error) reject(new Error(error));
                        resolve(body);
                    });
                });
            };

            //FUNCION QUE CREAR EL ARRAY DE ADJUNTOS QUE VA EN CADA PAGINA DE ESTADO
            async function adjuntos(idPageEstado, labels) {
                try {
                    var subRutaArray = [];

                    fs.readdirSync(carpetaDocs).forEach(file => {
                        var rutaFi = carpetaDocs + file;
                        subRutaArray.push(rutaFi);
                    });


                    for (var m = 0; m < subRutaArray.length; m++) {
                        var ruta = subRutaArray[m];
                        fileUtils.comentar("/******** iConfluence::RUTA ==> " + ruta);
                        var bodyAtt = await crearAttachment(espacio, idPageEstado, ruta).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al crear attachment ==> " + error));
                        var iDAtt = bodyAtt.results[0].id;
                        var bodyData = utils.crearBodyLabels(iDAtt, labels);
                        options = utils.crearOpcionesLabel(urlConfluence, authC, idPageEstado, iDAtt, bodyData);
                        await crearLabelAttachment(options);
                        fs.unlinkSync(ruta);
                    }
                } catch{
                    throw error;
                }
            }

            //FUNCION QUE CREA LA FASE Y ADJUNTA LOS DOCUMENTOS
            async function crearPaginaPhase() {
                try {

                    var nombreEstado = fase(nEstado)

                    var titulo_fase = nombreEstado + ' ' + issuekey;
                    var labels = '';

                    var value = "<ac:structured-macro ac:name=\\\"attachments\\\"><ac:parameter ac:name=\\\"sortBy\\\">name</ac:parameter><ac:parameter ac:name=\\\"sortOrder\\\">ascending</ac:parameter></ac:structured-macro>";
                    // Crear Pagina de Fase
                    var bodyData = "";

                    /***************************  CREAMOS ETIQUETAS ******************************************/
                    //var etiquetasApp;
                    //var etiquetasSum;
                    //var etiquetaKey;
                    var etiquetaNApp;
                    var etiquetas;
                    var aplicacion = unhyphenate(componenteJira.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,''));
                    var resumen = unhyphenate(issuesummary.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,''));

                    etiquetaNApp = string_to_array(iniciales);

                    etiquetas = etiquetaNApp.concat(string_to_array(issuekey.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'')));
                    etiquetas = etiquetas.concat(string_to_array(nombreEstado.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'')));
                    etiquetas = etiquetas.concat(string_to_array(resumen));
                    etiquetas = etiquetas.concat(string_to_array(aplicacion));
                    etiquetas = etiquetas.concat(string_to_array('evolutivo'));

                    /*******************************************************************************************/

                    var labels = utils.crearLabels(etiquetas);

                    if (first == 1) {
                        bodyData = utils.crearBodyHijo(titulo_fase, espacio, idAncestor3, value, labels);
                        first = 0;
                    }
                    else {
                        bodyData = utils.crearBodyHijo(titulo_fase, espacio, idAncestor2, value, labels);
                    }
                    options = utils.crearOpciones(urlConfluence, authC, bodyData);
                    var idPageEstado;
                    await utils.getIDPagina(options).then(iD => idPageEstado = iD).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al crear pagina Phase ==> " + error));
                    fileUtils.comentar("/******** iConfluence::PAGINA FASE ==> " + idPageEstado);
                    await adjuntos(idPageEstado, labels);
                    //}
                } catch (error) {
                    fileUtils.comentar("/******** iConfluence::ERROR en #crearPaginaPhase()# ==> " + error);
                    throw error;
                }
            }

            //FUNCION QUE CREA LAS PAGINAS ISSUE
            async function crearPaginaIssue() {
                try {

                    var value = issuedesc;

                    value += '</p><br></br><p><ac:structured-macro ac:name=\\\"jira\\\" ac:schema-version=\\\"1\\\"><ac:parameter ac:name=\\\"key\\\">' + issuekey + '</ac:parameter></ac:structured-macro></p>';
                    value += '<br></br><p><ac:structured-macro ac:name=\\\"info\\\"><ac:parameter ac:name=\\\"icon\\\">true</ac:parameter><ac:rich-text-body><p>El evolutivo contiene la siguiente documentaci\xF3n organizada en las diferentes fases de aportaci\xF3n de documentaci\xF3n del flujo de trabajo.</p></ac:rich-text-body></ac:structured-macro></p>';
                    value += '<p><ac:structured-macro ac:name=\\\"pagetree\\\"><ac:parameter ac:name=\\\"root\\\"><ac:link><ri:page ri:content-title=\\\"' + issuePage + '\\\"/></ac:link></ac:parameter><ac:parameter ac:name=\\\"startDepth\\\">1</ac:parameter></ac:structured-macro>';


                    /***************************  CREAMOS ETIQUETAS ******************************************/
                    //var etiquetasApp;
                    //var etiquetasSum;
                    //var etiquetaKey;
                    var etiquetaNApp;
                    var etiquetas;
                    var aplicacion = unhyphenate(componenteJira.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,''));
                    var resumen = unhyphenate(issuesummary.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,''));

                    etiquetaNApp = string_to_array(iniciales);

                    etiquetas = etiquetaNApp.concat(string_to_array(issuekey.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'')));
                    etiquetas = etiquetas.concat(string_to_array(resumen));
                    etiquetas = etiquetas.concat(string_to_array(aplicacion));
                    etiquetas = etiquetas.concat(string_to_array('evolutivo'));

                    /*******************************************************************************************/

                    var labels = utils.crearLabels(etiquetas);

                    // Crear Pagina de Issue
                    var bodyData = utils.crearBodyHijo(issuePage, espacio, idAncestor11, value, labels);
                    options = utils.crearOpciones(urlConfluence, authC, bodyData);
                    await utils.getIDPagina(options).then(iD => idAncestor3 = iD).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al crear pagina Issue ==> " + error));
                    first = 1;
                    await crearPaginaPhase();
                } catch (error) {
                    fileUtils.comentar("/******** iConfluence::ERROR en #crearPaginaIssue()# ==> " + error);
                    throw error;
                }
            }

            //FUNCION INICIAL UNA VEZ RECOGIDOS LOS ATTACHMENT. INVOCAR A CONFLUENCE Y REALIZAR PROCESO
            async function sendToConfluence(aplicacion) {
                try {
                    var phase = fase(nEstado) + " " + issuekey;
                    options = utils.crearOpcionesGET(urlConfluence, authC, aplicacion, espacio);  
                    await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor1 = iD).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al obtener pagina de aplicacion ==> " + error));  
                    if (idAncestor1 != -1) { //App existe                         
                        options = utils.crearOpcionesGET(urlConfluence, authC, titulo, espacio);   
                        await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor11 = iD).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al obtener pagina de modulo ==> " + error));           
                        options = utils.crearOpcionesGET(urlConfluence, authC, issuePage, espacio);
                        await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor2 = iD).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al obtener pagina de issue ==> " + error));  
                        if (idAncestor2 != -1) { //Página Issue existe
                            options = utils.crearOpcionesGET(urlConfluence, authC, phase, espacio);
                            await utils.getIDPaginaPorTitutlo(options).then(iD => idAncestor3 = iD).catch(error => fileUtils.comentar("/******** iConfluence::ERROR al obtener pagina de fase ==> " + error));
                            if (idAncestor3 != -1) { //Página Phase existe
                                // No creas páginas
                                var nombreEstado = fase(nEstado)
                                var labels = '';

                                /***************************  CREAMOS ETIQUETAS ******************************************/
                                //var etiquetasApp;
                                //var etiquetasSum;
                                //var etiquetaKey;
                                var etiquetaNApp;
                                var etiquetas;
                                var aplicacion = unhyphenate(componenteJira.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,''));
                                var resumen = unhyphenate(issuesummary.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,''));

                                etiquetaNApp = string_to_array(iniciales);

                                etiquetas = etiquetaNApp.concat(string_to_array(issuekey.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'')));
                                etiquetas = etiquetas.concat(string_to_array(nombreEstado.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,'')));
                                etiquetas = etiquetas.concat(string_to_array(resumen));
                                etiquetas = etiquetas.concat(string_to_array(aplicacion));
                                etiquetas = etiquetas.concat(string_to_array('evolutivo'));

                                /*******************************************************************************************/

                                var labels = utils.crearLabels(etiquetas);
                                adjuntos(idAncestor3, labels);
                            } else {
                                // Crear Fase
                                await crearPaginaPhase();
                            }
                        } else {
                            // Crear Issue y Fase
                            await crearPaginaIssue();
                        }

                    } else {
                        fileUtils.comentar("/******** iConfluence::ERROR al obtener pagina de aplicacion ==> " + error);
                    }
                } catch (error) {
                    fileUtils.comentar("/******** iConfluence::ERROR en #sendToConfluence()# ==> " + error);
                    throw error;
                }
            }

            //############################################ MAIN ###################################################

            /*
            Preparamos la request al API de JIRA con la que cogeremos los datos del ATTACHMENT 
            La Auth se hara mediante la creacion de un token */
            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/2/issue/' + issuekey,
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };
            request(options, function (error, response, body) {

                if (error) throw new Error(error);

	  		    /*
	  		    Procesamos el texto para coger la etiqueta del attach solamente
	  		    Despues lo convertiremos en JSON
	  		    Buscaremos entre todos los attach los que tengan el nombre de documentacion tecnica
	  		    */

                var ini = body.match(/attachment/i);
                var fin = body.match(/summary/i);
                var attachmentJSON = '{' + body.substring(ini.index - 1, fin.index - 2) + '}';
                var newBody = JSON.parse(attachmentJSON);

	 		    /*
	 		    Crearemos un array para cargar los documentos del ticket, los cuales procesaremos posteriormente.
	 		    */

                for (var i = 0; i < newBody.attachment.length; i++) {
                    var attachs = newBody.attachment[i];
                    attachArray.push(attachs);
                }

                var rutaArray = [];

                /*
                Recogemos todos los attachment y los almacenamos en carpeta docs
                */

                for (var j = 0; j < attachArray.length; j++) {
                    var elFichero = attachArray[j];
                    options = {
                        method: 'GET',
                        url: elFichero.content,
                        headers: {
                            'Authorization': auth,
                            'Content-Type': 'application/json'
                        }
                    };

                    var rutaFichero = carpetaDocs + elFichero.filename;

                    rutaArray.push(rutaFichero);

                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                    }).pipe(fs.createWriteStream(rutaFichero));
                }

            });
            /*************************************************************************************************** */
            /* {
             * Obtenemos los valores de aplicacion y modulo con el objetivo de buscar la pagina correspondiente donde incluir los documentos
             */

            try {
                var fileJSON = JSON.parse(fs.readFileSync('./utils_json/aplicaciones.json', 'utf8'));
                var salir = false;
                var i = 0;
                fileUtils.comentar("/******** iConfluence::JIRA componente ==> " + componenteJira);
                while (i < fileJSON.aplicaciones.length && salir == false) {
                    if (fileJSON.aplicaciones[i].componente == componenteJira) {
                        titulo = pGE + ' ' + fileJSON.aplicaciones[i].iniciales_mod;
                        aplicacion = fileJSON.aplicaciones[i].aplicacion;
                        modulo = fileJSON.aplicaciones[i].modulo;
                        iniciales = fileJSON.aplicaciones[i].iniciales_app;
                        iniciales_mod = fileJSON.aplicaciones[i].iniciales_mod;
                        componente = fileJSON.aplicaciones[i].componente;
                        grupo = fileJSON.aplicaciones[i].grupo;
                        salir = true;
                    }
                    i++;
                }
            } catch {
                console.log("/******** iConfluence::ERROR al procesar la informacion del modulo y aplicacion ==> " + error);
                throw error;
            }
            fileUtils.comentar("/******** iConfluence::JIRA aplicación  ==> " + aplicacion);
            fileUtils.comentar("/******** iConfluence::JIRA módulo ==> " + modulo);
            /************************************************************************************************************/
            /**************************************************************************************************************/
            //Llamada a la función principal que desencadena el proceso de integracion

            sendToConfluence(aplicacion).then(() => {
                fileUtils.comentar("/******** iConfluence:: REALIZADO ENVIO DE DOCUMENTOS A CONFLUENCE");
                /* si todo ha ido bien, se eliminan los documentos del ticket */
                try {
                    for (var j = 0; j < attachArray.length; j++) {
                        var elFichero = attachArray[j];
                        options = {
                            method: 'DELETE',
                            url: urlJira + '/rest/api/3/attachment/' + elFichero.id,
                            headers: {
                                'Authorization': auth,
                                'Content-Type': 'application/json'
                            }
                        };

                        request(options, function (error, response, body) {
                            if (error) throw new Error(error);
                        });
                    }
                } catch{
                    fileUtils.comentar("/******** iConfluence::ERROR al eliminar los adjuntos del ticket ==> " + error);
                    throw error;
                }
                fileUtils.comentar("/******** iConfluence::FIN DE PROCESO");
                res.sendStatus(200);
            }).catch((error) => {
                fileUtils.comentar("/******** iConfluence::ERROR EN EL ENVIO DE DOCUMENTOS A CONFLUENCE ==> " + error);
                //res.sendStatus(500);
            });
            /**************************************************************************************************************/
            /**************************************************************************************************************/

            //###################################################################################################

        } catch (error) {
            console.log("::ERROR. El proceso de envio ha fallado::" + error);
        }
    });

    /**
     * Route serving link to view in postfunction workflows the confluence-integration option. 
     * @name get/view
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/view', addon.authenticate(), function (req, res) {
        res.render('workflow/view', {
            id: req.param('id')
        });
    });

    /**
     * Route serving link to edit in postfunction workflows the confluence-integration option. 
     * @name get/edit
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/edit', addon.authenticate(), function (req, res) {
        res.render('workflow/edit', {
            id: req.param('id')
        });
    });

    /**
     * Route serving link to create in postfunction workflows the confluence-integration option. 
     * @name get/create
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/create', function (req, res) {
        res.render('workflow/create', {

        });
    });

    /**
     * Route serving link to allow access to CMDB information in issue detail screen.
     * @name get/web-panel-cmdb
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/web-panel-cmdb', addon.authenticate(), (req, res) => {
        try {
            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/3/issue/' + req.param('issueKey') + '/properties/iCMDB_active_issue_property',
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                //console.log(
                //    'Response: ' + response.statusCode + ' ' + response.statusMessage
                //);
                let json = JSON.parse(body);
                let summary = json.value;
                //console.log(email);
                var options = {
                    method: 'GET',
                    url: urlJira + '/rest/api/2/search?jql=Summary~' + '\"' + summary + '\"',
                    headers: {
                        'Authorization': auth,
                        'Accept': 'application/json'
                    }
                };
                request(options, function (error, response, body) {
                    if (error) throw new Error(error);
                    //console.log(
                    //    'Response: ' + response.statusCode + ' ' + response.statusMessage
                    //);
                    try {
                        let json2 = JSON.parse(body)
                        var arr = json2.issues;
                        let f = JSON.stringify(arr);
                        f = f.slice(1);
                        f = f.slice(0, f.length - 1);
                        json2 = JSON.parse(f);
                        // Rendering a template is easy; the render method takes two params:
                        // name of template and a json object to pass the context in.
                        res.render('documents', {
                            title: 'Equipo del usuario',
                            idEquipo: json2.fields.summary,
                            idIssue: json2.key,
                            issueType: json2.fields.customfield_10156.value,
                            status: json2.fields.status.name,
                            marca: json2.fields.customfield_10140.value, //fabricante
                            procesador: json2.fields.customfield_10147.value, //modelo CPU
                            so: json2.fields.customfield_10172.value // Sistema Operativo
                        });
                    } catch (error) {
                        res.render('documents_error', {
                        });
                    }
                });
            });
        } catch (error) {
            res.render('documents_error', {
            });
        }
    });

    /**
     * Route serving link to allow access to CMDB form in portal depends on Request Type.
     * @name get/sd-portal-request-create-property-content
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/sd-portal-request-create-property-content', addon.authenticate(), (req, res) => {

        try {
            /******************************************************************************
            Inicializaci�n de variables
            *******************************************************************************/
            var decoded = jwt.decode(req.param('jwt'));
            var user = decoded.sub;

            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/3/user/search',
                qs: { accountId: user },
                headers:
                {
                    'Connection': 'keep-alive',
                    'Cache-Control': 'no-cache',
                    'Authorization': auth,
                    'Content-Type': 'application/json'
                }
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                var json = JSON.parse(body);
                var key = json[0].key;
                //fileUtils.comentar('JSON TAB:' + JSON.stringify(json));
                //email = email.replace("@", "\@");
                var options = {
                    method: 'GET',
                    //url: urlJira + '/rest/api/2/search?jql=project=NORTEGAS CMDB TEST and Reporter=' + '\"' + key + '\"',
                    url: urlJira + '/rest/api/2/search?jql=project=\'NORTEGAS CMDB\' and Reporter=' +key ,
                    headers: {
                        'Authorization': auth,
                        'Accept': 'application/json'
                    }
                };
                request(options, function (error, response, body) {
                    if (error) throw new Error(error);
                    //console.log(
                    //    'Response: ' + response.statusCode + ' ' + response.statusMessage
                    //);
                    try {
                        let json2 = JSON.parse(body);
                        let j = JSON.stringify(json2);
                        var hay = false;
                        if (json2.total > 0) hay = true;
                        //fileUtils.comentar('JSON TAB:' + JSON.stringify(json2));
                        if ((req.param('idRequestType') == 18 || req.param('idRequestType') == 19 || req.param('idRequestType') == 21 || req.param('idRequestType') == 23 || req.param('idRequestType') == 87 || req.param('idRequestType') == 30 || req.param('idRequestType') == 32 || req.param('idRequestType') == 85 || req.param('idRequestType') == 84 || req.param('idRequestType') == 46))

                            res.render('form', {
                                equipos: json2,
                                hay: hay
                            })
                        else
                            res.render('form_nothing', {
                            })
                    } catch (error) {
                        res.render('form_nothing', {
                        });
                    }
                });

            });
        } catch (error) {
            res.render('form_nothing', {
            })
        }
    });

    /**
     * Route serving link to allow access to ANSs tab un issue detail screen.
     * @name get/iSLA-tab
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/iSLA-tab', addon.authenticate(), (req, res) => {
        try {
            /******************************************************************************
            Inicializaci�n de variables
            Accede a los datos de la issue y actualiza las entidades
            *******************************************************************************/
            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/2/issue/' + req.param('issueKey'),
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };

            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                issueJSON = JSON.parse(body);
                issuekey = issueJSON.key;
                createdDate = issueJSON.fields.created;
                updatedDate = issueJSON.fields.updated;
                dfDate = moment(issueJSON.fields.customfield_10094).add(1439, 'minutes'); //Fecha Diseño funcional planificada
                dtDate = moment(issueJSON.fields.customfield_10061).add(1439, 'minutes'); // Fecha Diseño tecnico planificada
                impDate = moment(issueJSON.fields.customfield_10063).add(1439, 'minutes'); // Fecha desarrollo planificada
                //fileUtils.comentar('JSON TAB:' + JSON.stringify(issueJSON));
            });

            //********************************* FUNCIONES ***************************************************
            //***********************************************************************************************

            //################################# PROPIEDADES SLA DISE�O FUNCIONAL #############################
            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/3/issue/' + req.param('issueKey') + '/properties/slaDF',
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };
            request(options, function (error, response, body) {
                if (error) throw new Error(error);
                jsonDF = JSON.parse(body);
                //datos de respuesta

                //################################# PROPIEDADES SLA DISE�O T�CNICO #############################
                var options = {
                    method: 'GET',
                    url: urlJira + '/rest/api/3/issue/' + req.param('issueKey') + '/properties/slaDT',
                    headers: {
                        'Authorization': auth,
                        'Accept': 'application/json'
                    }
                };
                request(options, function (error, response, body) {
                    if (error) throw new Error(error);
                    jsonDT = JSON.parse(body);
                    //################################# PROPIEDADES SLA IMPLEMENTACI�N #############################
                    var options = {
                        method: 'GET',
                        url: urlJira + '/rest/api/3/issue/' + req.param('issueKey') + '/properties/slaIMP',
                        headers: {
                            'Authorization': auth,
                            'Accept': 'application/json'
                        }
                    };
                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);
                        jsonIMP = JSON.parse(body);
                        if (jsonDF.hasOwnProperty('errors')) {
                            fileUtils.comentar('No hay DF SLA');
                            var startDF = "";
                            var goalDF = "";
                            var closeTimeDF = "";
                            var restanteDF = "";
                            var restanteDFDetail = "";
                            var iconDF = '../images/pause_black.jpg';
                        } else {
                            moment.locale('es');
                            var breachedDF = jsonDF.value.breached;
                            var pausedDF = jsonDF.value.paused;
                            var closedDF = jsonDF.value.closed;
                            var closeTimeDF = jsonDF.value.closeTime.jira;
                            var withinCalendarHoursDF = jsonDF.value.withinCalendarHours;
                            var withinWCalendarHoursDF = jsonDF.value.withinWCalendarHours;
                            var startDF = jsonDF.value.startTime.jira;
                            var goalDF = dfDate;
                            if ((!breachedDF) && (moment().isBefore(dfDate))) {
                                withinCalendarHoursDF = 1;
                                withinWCalendarHoursDF = utils.inWorkingHours();
                                var bsiDF = utils.build_SLA_Information(startDF, goalDF, closeTimeDF, pausedDF, breachedDF, withinCalendarHoursDF, withinWCalendarHoursDF, 0);
                                var jsDF = JSON.parse(bsiDF);
                                utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDF, 'slaDF')
                                restanteDF = jsDF.remainingTime.friendly
                                restanteDFDetail = moment(jsDF.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                startDF = jsDF.startTime.friendly;
                                //goalDF = jsDF.breachTime.friendly;
                                goalDF = moment(jsDF.breachTime.jira).format("ll");
                                closeTimeDF = '';
                                rest = Math.abs(utils.getBusinessDays(moment().format(), jsonDF.value.breachTime.jira));
                                rest++;
                                restanteDF = "Dentro de " + rest + " d\xEDas";
                                iconDF = '../images/clock_green.jpg';
                            } else
                                if ((!breachedDF) && (moment().isAfter(dfDate))) {
                                    withinCalendarHoursDF = 0;
                                    withinWCalendarHoursDF = utils.inWorkingHours();;
                                    var bsiDF = utils.build_SLA_Information(startDF, goalDF, closeTimeDF, pausedDF, breachedDF, withinCalendarHoursDF, withinWCalendarHoursDF, 0);
                                    var jsDF = JSON.parse(bsiDF);
                                    utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDF, 'slaDF');
                                    restanteDF = jsDF.remainingTime.friendly;
                                    restanteDFDetail = moment(jsDF.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                    startDF = jsDF.startTime.friendly;
                                    //goalDF = jsDF.breachTime.friendly;
                                    goalDF = moment(jsDF.breachTime.jira).format("ll");
                                    closeTimeDF = '';
                                    rest = Math.abs(utils.getBusinessDays(jsonDF.value.breachTime.jira), moment().format());
                                    rest++;
                                    restanteDF = "Hace " + rest + " d\xEDas";
                                    iconDF = '../images/clock_red.jpg';
                                } else
                                    if ((breachedDF) && (moment(closeTimeDF).isBefore(dfDate))) {
                                        withinCalendarHoursDF = 1;
                                        withinWCalendarHoursDF = utils.inWorkingHours();
                                        var bsiDF = utils.build_SLA_Information(startDF, goalDF, closeTimeDF, pausedDF, breachedDF, withinCalendarHoursDF, withinWCalendarHoursDF, 1);
                                        var jsDF = JSON.parse(bsiDF);
                                        utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDF, 'slaDF');
                                        startDF = jsDF.startTime.friendly;
                                        goalDF = moment(jsDF.breachTime.jira).format("ll");
                                        closeTimeDF = moment(jsDF.closeTime.jira).format("ll");
                                        iconDF = '../images/breached_green.jpg';
                                        //restanteDF = "----";
                                        restanteDF = "Finalizado " + rest;
                                        var a = moment(jsDF.closeTime.jira);
                                        var b = moment(jsDF.breachTime.jira);
                                        var rest = moment.duration((b - a)).asDays();
                                        //rest = Math.ceil(rest) + " d\xEDas antes";
                                        rest = Math.abs(utils.getBusinessDays(a, b));
                                        rest++;
                                        rest = rest + " d\xEDas antes";
                                        restanteDFDetail = "Finalizado " + rest;
                                        restanteDF = "Finalizado " + rest;
                                    } else
                                        if ((breachedDF) && (moment(closeTimeDF).isAfter(dfDate))) {
                                            withinCalendarHoursDF = 0;
                                            withinWCalendarHoursDF = utils.inWorkingHours();
                                            var bsiDF = utils.build_SLA_Information(startDF, goalDF, closeTimeDF, pausedDF, breachedDF, withinCalendarHoursDF, withinWCalendarHoursDF, 1);
                                            var jsDF = JSON.parse(bsiDF);
                                            utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDF, 'slaDF');
                                            restanteDF = jsDF.remainingTime.friendly;
                                            startDF = jsDF.startTime.friendly;
                                            //goalDF = jsDF.breachTime.friendly;
                                            goalDF = moment(jsDF.breachTime.jira).format("ll");
                                            closeTimeDF = moment(jsDF.closeTime.jira).format("ll");
                                            iconDF = '../images/breached_red.jpg'
                                            //restanteDF = "----";
                                            var a = moment(jsDF.closeTime.jira);
                                            var b = moment(jsDF.breachTime.jira);
                                            var rest = moment.duration((a - b)).asDays();
                                            //rest = Math.ceil(rest) + " d\xEDas despu\xE9s";
                                            rest = Math.abs(utils.getBusinessDays(a, b));
                                            rest++;
                                            rest = rest + " d\xEDas despu\xE9s";
                                            restanteDFDetail = "Finalizado " + rest;
                                            restanteDF = "Finalizado " + rest;
                                        }
                        }
                        if (jsonDT.hasOwnProperty('errors')) {
                            fileUtils.comentar('No hay DT SLA');
                            var startDT = "";
                            var goalDT = "";
                            var closeTimeDT = "";
                            var restanteDT = "";
                            var restanteDTDetail = "";
                            var iconDT = '../images/pause_black.jpg';
                        } else {
                            moment.locale('es');
                            var breachedDT = jsonDT.value.breached;
                            var pausedDT = jsonDT.value.paused;
                            var closedDT = jsonDT.value.closed;
                            var closeTimeDT = jsonDT.value.closeTime;
                            var withinCalendarHoursDT = jsonDT.value.withinCalendarHours;
                            var withinWCalendarHoursDT = jsonDT.value.withinWCalendarHours;
                            startDT = jsonDT.value.startTime.jira;
                            closeTimeDT = jsonDT.value.startTime.jira;
                            goalDT = dtDate;
                            if ((!breachedDT) && (moment().isBefore(dtDate))) {
                                withinCalendarHoursDT = 1;
                                withinWCalendarHoursDT = utils.inWorkingHours();;
                                var bsiDT = utils.build_SLA_Information(startDT, goalDT, closeTimeDT, pausedDT, breachedDT, withinCalendarHoursDT, withinWCalendarHoursDT, 0);
                                var jsDT = JSON.parse(bsiDT);
                                utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDT, 'slaDT');
                                restanteDT = jsDT.remainingTime.friendly;
                                restanteDTDetail = moment(jsDT.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                startDT = jsDT.startTime.friendly;
                                closeTimeDT = '';
                                goalDT = moment(jsDT.breachTime.jira).format("ll");
                                rest = Math.abs(utils.getBusinessDays(moment().format(), jsonDT.value.breachTime.jira));
                                rest++;
                                restanteDT = "Dentro de " + rest + " d\xEDas";
                                iconDT = '../images/clock_green.jpg';
                            } else
                                if ((!breachedDT) && (moment().isAfter(dtDate))) {
                                    withinCalendarHoursDT = 0;
                                    withinWCalendarHoursDT = utils.inWorkingHours();
                                    var bsiDT = utils.build_SLA_Information(startDT, goalDT, closeTimeDT, pausedDT, breachedDT, withinCalendarHoursDT, withinWCalendarHoursDT, 0);
                                    var jsDT = JSON.parse(bsiDT);
                                    utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDT, 'slaDT');
                                    restanteDT = jsDT.remainingTime.friendly;
                                    restanteDTDetail = moment(jsDT.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                    startDT = jsDT.startTime.friendly;
                                    goalDT = moment(jsDT.breachTime.jira).format("ll");
                                    closeTimeDT = '';
                                    rest = Math.abs(utils.getBusinessDays(jsonDT.value.breachTime.jira), moment().format());
                                    rest++;
                                    restanteDT = "Hace " + rest + " d\xEDas";
                                    iconDT = '../images/clock_red.jpg';
                                } else
                                    if ((breachedDT) && (moment(closeTimeDT).isBefore(dtDate))) {
                                        withinCalendarHoursDT = 1;
                                        withinWCalendarHoursDT = utils.inWorkingHours();
                                        var bsiDT = utils.build_SLA_Information(startDT, goalDT, closeTimeDT, pausedDT, breachedDT, withinCalendarHoursDT, withinWCalendarHoursDT, 1);
                                        var jsDT = JSON.parse(bsiDT);
                                        utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDT, 'slaDT');
                                        restanteDT = jsDT.remainingTime.friendly;
                                        restanteDTDetail = jsDT.breachTime.jira;
                                        startDT = jsDT.startTime.friendly;
                                        goalDT = moment(jsDT.breachTime.jira).format("ll");
                                        closeTimeDT = moment(jsDT.closeTime.jira).format("ll");
                                        iconDT = '../images/breached_green.jpg'
                                        //restanteDT = "----";
                                        var a = moment(jsDT.closeTime.jira);
                                        var b = moment(jsDT.breachTime.jira);
                                        var rest = moment.duration((b - a)).asDays();
                                        //rest = Math.ceil(rest) + " d\xEDas antes";
                                        rest = Math.abs(utils.getBusinessDays(a, b));
                                        rest++;
                                        rest = rest + " d\xEDas antes";
                                        restanteDTDetail = "Finalizado " + rest;
                                        restanteDT = "Finalizado " + rest;
                                    } else
                                        if ((breachedDT) && (moment(closeTimeDT).isAfter(dtDate))) {
                                            withinCalendarHoursDT = 0;
                                            withinWCalendarHoursDT = utils.inWorkingHours();
                                            var bsiDT = utils.build_SLA_Information(startDT, goalDT, closeTimeDT, pausedDT, breachedDT, withinCalendarHoursDT, withinWCalendarHoursDT, 1);
                                            var jsDT = JSON.parse(bsiDT);
                                            utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiDT, 'slaDT');
                                            restanteDT = jsDT.remainingTime.friendly;
                                            restanteDTDetail = jsDF.breachTime.jira;
                                            startDT = jsDT.startTime.friendly;
                                            goalDT = moment(jsDT.breachTime.jira).format("ll");
                                            closeTimeDT = moment(jsDT.closeTime.jira).format("ll");
                                            iconDT = '../images/breached_red.jpg'
                                            //restanteDT = "----";
                                            var a = moment(jsDT.closeTime.jira);
                                            var b = moment(jsDT.breachTime.jira);
                                            var rest = moment.duration((a - b)).asDays();
                                            //rest = Math.ceil(rest) + " d\xEDas despu\xE9s";
                                            rest = Math.abs(utils.getBusinessDays(a, b));
                                            rest++;
                                            rest = rest + " d\xEDas despu\xE9s";
                                            restanteDTDetail = "Finalizado " + rest;
                                            restanteDT = "Finalizado " + rest;
                                        }
                        }
                        if (jsonIMP.hasOwnProperty('errors')) {
                            fileUtils.comentar('No hay IMP SLA');
                            var startIMP = "";
                            var goalIMP = "";
                            var closeTimeIMP = "";
                            var restanteIMP = "";
                            var restanteIMPDetail = "";
                            var iconIMP = '../images/pause_black.jpg';
                        } else {
                            moment.locale('es');
                            var breachedIMP = jsonIMP.value.breached;
                            var pausedIMP = jsonIMP.value.paused;
                            var closedIMP = jsonIMP.value.closed;
                            var closedTimeIMP = jsonIMP.value.closeTime;
                            var withinCalendarHoursIMP = jsonIMP.value.withinCalendarHours;
                            var withinWCalendarHoursIMP = jsonIMP.value.withinWCalendarHours;
                            startIMP = jsonIMP.value.startTime.jira;
                            closeTimeIMP = jsonIMP.value.closeTime.jira;
                            goalIMP = impDate;
                            if ((!breachedIMP) && (moment().isBefore(impDate))) {
                                withinCalendarHoursIMP = 1;
                                withinWCalendarHoursIMP = utils.inWorkingHours();
                                var bsiIMP = utils.build_SLA_Information(startIMP, goalIMP, closeTimeIMP, pausedIMP, breachedIMP, withinCalendarHoursIMP, withinWCalendarHoursIMP, 0);
                                var jsIMP = JSON.parse(bsiIMP);
                                utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiIMP, 'slaIMP');
                                restanteIMP = jsIMP.remainingTime.friendly;
                                restanteIMPDetail = moment(jsIMP.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                startIMP = jsIMP.startTime.friendly;
                                closeTimeIMP = '';
                                goalIMP = moment(jsIMP.breachTime.jira).format("ll");
                                rest = Math.abs(utils.getBusinessDays(moment().format(), jsonIMP.value.breachTime.jira));
                                rest++;
                                restanteIMP = "Dentro de " + rest +" d\xEDas";
                                iconIMP = '../images/clock_green.jpg';
                            } else
                                if ((!breachedIMP) && (moment().isAfter(impDate))) {
                                    withinCalendarHoursIMP = 0;
                                    withinWCalendarHoursIMP = utils.inWorkingHours();
                                    var bsiIMP = utils.build_SLA_Information(startIMP, goalIMP, closeTimeIMP, pausedIMP, breachedIMP, withinCalendarHoursIMP, withinWCalendarHoursIMP, 0);
                                    var jsIMP = JSON.parse(bsiIMP);
                                    utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiIMP, 'slaIMP');
                                    restanteIMP = jsIMP.remainingTime.friendly;
                                    restanteIMPDetail = moment(jsIMP.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                    startIMP = jsIMP.startTime.friendly;
                                    closeTimeIMP = '';
                                    goalIMP = moment(jsIMP.breachTime.jira).format("ll");
                                    rest = Math.abs(utils.getBusinessDays(jsonIMP.value.breachTime.jira), moment().format());
                                    rest++;
                                    restanteIMP = "Hace " + rest + " d\xEDas";
                                    iconIMP = '../images/clock_red.jpg'
                                } else
                                    if ((breachedIMP) && (moment(closeTimeIMP).isBefore(impDate))) {
                                        withinCalendarHoursIMP = 1;
                                        withinWCalendarHoursIMP = utils.inWorkingHours();
                                        var bsiIMP = utils.build_SLA_Information(startIMP, goalIMP, closeTimeIMP, pausedIMP, breachedIMP, withinCalendarHoursIMP, withinWCalendarHoursIMP, 1);
                                        var jsIMP = JSON.parse(bsiIMP);
                                        utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiIMP, 'slaIMP');
                                        restanteIMP = jsIMP.remainingTime.friendly;
                                        restanteIMPDetail = moment(jsIMP.breachTime.jira).format("dddd, D MMMM YYYY, HH:mm");
                                        startIMP = jsIMP.startTime.friendly;
                                        closeTimeIMP = moment(jsIMP.closeTime.jira).format("ll");
                                        goalIMP = moment(jsIMP.breachTime.jira).format("ll");
                                        iconIMP = '../images/breached_green.jpg'
                                        //restanteIMP = "----";
                                        var a = moment(jsIMP.closeTime.jira);
                                        var b = moment(jsIMP.breachTime.jira);
                                        var rest = moment.duration((b - a)).asDays();
                                        //rest = Math.ceil(rest) + " d\xEDas antes";
                                        rest = Math.abs(utils.getBusinessDays(a, b));
                                        rest++;
                                        rest = rest + " d\xEDas antes";
                                        restanteIMPDetail = "Finalizado " + rest;
                                        restanteIMP = "Finalizado " + rest;
                                    } else
                                        if ((breachedIMP) && (moment(closeTimeIMP).isAfter(impDate))) {
                                            withinCalendarHoursIMP = 0;
                                            withinWCalendarHoursIMP = utils.inWorkingHours();
                                            var bsiIMP = utils.build_SLA_Information(startIMP, goalIMP, closeTimeIMP, pausedIMP, breachedIMP, withinCalendarHoursIMP, withinWCalendarHoursIMP, 1);
                                            var jsIMP = JSON.parse(bsiIMP);
                                            utils.put_SLA_into_entity(urlJira, issuekey, auth, bsiIMP, 'slaIMP');
                                            restanteIMP = jsIMP.remainingTime.friendly;
                                            startIMP = jsIMP.startTime.friendly;
                                            closeTimeIMP = moment(jsIMP.closeTime.jira).format("ll");
                                            goalIMP = moment(jsIMP.breachTime.jira).format("ll");
                                            iconIMP = '../images/breached_red.jpg'
                                            //restanteIMP = "----";
                                            var a = moment(jsIMP.closeTime.jira);
                                            var b = moment(jsIMP.breachTime.jira);
                                            var rest = moment.duration((a - b)).asDays();
                                            //rest = Math.ceil(rest) + " d\xEDas despu\xE9s";
                                            rest = Math.abs(utils.getBusinessDays(a, b));
                                            rest++;
                                            rest = rest + " d\xEDas despu\xE9s";
                                            restanteIMPDetail = "Finalizado " + rest;
                                            restanteIMP = "Finalizado " + rest;
                                        }
                        };
                        //######################## RENDERIZAR DATOS EN PANTALLA ##########################
                        res.render('slaRes', {
                            iconDF: iconDF,
                            startDF: closeTimeDF,
                            goalDF: goalDF,
                            faseDF: 'Dise\xF1o Funcional',
                            restanteDF: restanteDF,
                            restanteDFDetail: restanteDFDetail,
                            iconDT: iconDT,
                            startDT: closeTimeDT,
                            goalDT: goalDT,
                            faseDT: 'Dise\xF1o T\xE9cnico',
                            restanteDT: restanteDT,
                            restanteDTDetail: restanteDTDetail,
                            iconIMP: iconIMP,
                            startIMP: closeTimeIMP,
                            goalIMP: goalIMP,
                            faseIMP: 'Implementaci\xF3n',
                            restanteIMP: restanteIMP,
                            restanteIMPDetail: restanteIMPDetail,
                        });
                    });
                    //##############################################################################################
                });
                //##############################################################################################
            });
            //######################################################################################################
        } catch { console.log("::ERROR /iSLA-tab::" + error); }
    });

    /**
     * Route serving link to control SLA flow. It's triggered through postfunction workflows.
     * @name post/iSLA-wpf
     * @async
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.post('/iSLA-wpf', addon.authenticate(), (req, res) => {
        try {

            /******************************************************************************
            Inicializaci�n de variables
            *******************************************************************************/
            const issueJSON = req.body;
            const issuekey = issueJSON.issue.key;
            const transicion = issueJSON.transition.transitionId;
            const createdDate = issueJSON.issue.fields.created;
            const updatedDate = issueJSON.issue.fields.updated;
            const dfDate = moment(issueJSON.issue.fields.customfield_10094).add(1439, 'minutes');
            const dtDate = moment(issueJSON.issue.fields.customfield_10061).add(1439, 'minutes');
            const impDate = moment(issueJSON.issue.fields.customfield_10063).add(1439, 'minutes');
            const status = issueJSON.issue.fields.status.id;
            const statusName = issueJSON.issue.fields.status.name;
            const fromStatus = issueJSON.transition.from_status;
            //fileUtils.comentar('JSON WPF:' + JSON.stringify(issueJSON));

            /******************************************************************************
            Funciones
            *******************************************************************************/


            //############################################ MAIN ###################################################
            //######  En las transiciones se establecen los estados de cada SLA y se actualizan las propiedades####
            fileUtils.comentar("$$$$$$$$$$ Transición $$$$$$$$$" + transicion);
            switch (transicion) {
                case 421:
                    // Aprobar planificaci�n. Start DF
                    utils.start_SLA(urlJira, issuekey, auth, 'slaDF', updatedDate, dfDate);
                    break;
                case 921:
                    // Aprobar dise�o funcional DF. Start DT                   
                    utils.start_SLA(urlJira, issuekey, auth, 'slaDT', updatedDate, dtDate);
                    break;
                case 241:
                    // Aprobar dise�o t�cnico TI. Start IMP
                    utils.start_SLA(urlJira, issuekey, auth, 'slaIMP', updatedDate, impDate);
                    break;
                case 221:
                    // Entregar dise�o funcional. End DF
                    utils.end_SLA(urlJira, issuekey, auth, 'slaDF', updatedDate, dfDate);
                    break;
                case 431:
                    // Entregar dise�o t�cnico. End DT
                    utils.end_SLA(urlJira, issuekey, auth, 'slaDT', updatedDate, dtDate);
                    break;
                case 461:
                    // Pasar a pruebas TI. End IMP
                    utils.end_SLA(urlJira, issuekey, auth, 'slaIMP', updatedDate, impDate);
                    break;
                case 321:
                    // Rechazar DF
                    utils.reject_SLA(urlJira, issuekey, auth, 'slaRDF', updatedDate, dfDate);
                    break;
                case 441:
                    // Rechazar DT
                    utils.reject_SLA(urlJira, issuekey, auth, 'slaRDT', updatedDate, dtDate);
                    break;
                case 471:
                    // Rechazar IMP
                    utils.reject_SLA(urlJira, issuekey, auth, 'slaRIMP', updatedDate, impDate);
                    break;
                default:
                    // code block
                    fileUtils.comentar('NO ENCUENTRA TRANSICI�N');
                    return '';
            }
            res.sendStatus(200);
            fileUtils.comentar('FIN DE PROCESO');
            //###################################################################################################

        } catch (error) {
            console.log("::ERROR app.post::" + error);
        }
    });

    /**
     * Route serving link to view in postfunction workflows the iSLA option. 
     * @name post/view_isla
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/view_isla', addon.authenticate(), function (req, res) {
        res.render('workflow/view_isla', {
            id: req.param('id')
        });
    });

    /**
     * Route serving link to edit in postfunction workflows the iSLA option. 
     * @name get/edit_isla
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/edit_isla', addon.authenticate(), function (req, res) {
        res.render('workflow/edit_isla', {
            id: req.param('id')
        });
    });

    /**
     * Route serving link to create in postfunction workflows the iSLA option. 
     * @name get/create_isla
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/create_isla', function (req, res) {
        res.render('workflow/create_isla', {

        });
    });

    /**
     * Route serving link to create in postfunction workflows the remedy-integration option.
     * @name post/remedy-integration
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.post('/remedy-integration', addon.authenticate(), async (req, res) => {
        try {

            /******************************************************************************
            Inicializaci�n de variables
            *******************************************************************************/
            const issueJSON = req.body;
            const issuekey = issueJSON.issue.key;
            const issuesummary = issueJSON.issue.fields.summary;
            const issuePage = issuekey + ': ' + issuesummary;
            const nEstado = issueJSON.issue.fields.status.id;
            var estadoUrl;
            //var nombreEstado = fase(nEstado)
            if (nEstado == '10059'){
                //estadoUrl = 'Transporte+Integraci+n+';
                estadoUrl = 'Transporte Integraci\xF3n ';
            }else{
                //estadoUrl = 'Transporte+Producci+n+';
                estadoUrl = 'Transporte Producci\xF3n ';
            }
            var options = utils.crearOpcionesGET(urlConfluence, auth, issuePage, espacio);
            fileUtils.comentar('OPTIONS:' + JSON.stringify(options));
            await utils.getIDPaginaPorTitutlo(options).then(iD => idPage = iD).catch(error => fileUtils.comentar('ERROR: ' + error));
            var urlR = 'Documentacion: ' + urlConfluence + '/spaces/' + espacio + '/pages/' + idPage + '/' + estadoUrl + issuekey;
            fileUtils.comentar('URL CONFLUENCE:' + urlR);
            //fileUtils.comentar('JSON WPF:' + JSON.stringify(issueJSON));

            var remedy = JSON.parse(fs.readFileSync('./utils_json/remedy.json', 'utf8'));

            var bodyCreate = `{
                    "values":
                    {
                        "Login_ID": "${remedy.values.Login_ID}",
                        "Reported Source": "${remedy.values.Reported_Source}",
                        "Service_Type": "${remedy.values.Service_Type}",
                        "Categorization Tier 1":"${remedy.values.Categorization_Tier_1}" ,
                        "Product Categorization Tier 1":"${remedy.values.Product_Categorization_Tier_1}",
                        "Product Categorization Tier 2": "${remedy.values.Product_Categorization_Tier_2}",
                        "Product Categorization Tier 3": "${remedy.values.Product_Categorization_Tier_3}",
                        "Description": "${remedy.values.Description}",
                        "Vendor Ticket Number": "${issuekey}",
                        "Detailed_Decription": "${urlR}"
                    }
                }`;

            //############################################ MAIN ###################################################
            //######  Solicitud de Ticket en remedy en dos pasos. 1: Solicitud de token, 2:envio de peticion ####

            //@@@@@@@@@@@@@@@@@@@@@@@@   solicitud de token @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            fileUtils.comentar('SOLICITAR TOKEN:');
            var token = await utils.getTokenRemedy(remedy.login.urlLogin, remedy.login.username, remedy.login.password);
            fileUtils.comentar('CREAR TICKET:' + token);
            //@@@@@@@@@@@@@@@@@@@@@@@@   Creacion de ticket @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            var res = await utils.createTicketRemedy(remedy.login.urlCreate, token, bodyCreate);

            //@@@@@@@@@@@@@@@@@@@@@@@@   obtencion de incidencia de incidencia @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            fileUtils.comentar('OBTENER INCIDENCIA:' + JSON.stringify(res));
            var br = await utils.createIncidentRemedy(res.headers.location, token);
            var obj = JSON.parse(br)
            //var incident = JSON.stringify(br);
            var incident = JSON.stringify(obj.values["Incident Number"]);

            fileUtils.comentar('ACTUALIZAR ISSUE:'+incident);
            //@@@@@@@@@@@@@@@@@@@@@@@@   actualizar issue @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            b = await utils.editIssue(urlJira, auth, issuekey, incident, nEstado);

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            fileUtils.comentar('FIN DE PROCESO');
            //###################################################################################################

        } catch (error) {
            console.log("::ERROR app.post::" + error);
        }
    });

    /**
     * Route serving link to view in postfunction workflows the Remedy integration option. 
     * @name post/view_ao
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/view-ao', addon.authenticate(), function (req, res) {
        res.render('workflow/view_ao', {
            id: req.param('id')
        });
    });

    /**
     * Route serving link to edit in postfunction workflows the Remedy integration option.
     * @name get/edit_ao
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/edit-ao', addon.authenticate(), function (req, res) {
        res.render('workflow/edit_ao', {
            id: req.param('id')
        });
    });

    /**
     * Route serving link to create in postfunction workflows the Remedy integration option.
     * @name get/create_ao
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/create-ao', function (req, res) {
        res.render('workflow/create_ao', {

        });
    });
    /**
     * Route serving link to trigger in postfunction workflows the create remaining Budget option.
     * @name get/create-budget
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.post('/budget-wpf', addon.authenticate(), async (req, res) => {
        try {

            /******************************************************************************
            Inicializaci�n de variables
            *******************************************************************************/
            const issueJSON = req.body;
            const issuekey = issueJSON.issue.key;
            var aplicacion = issueJSON.issue.fields.customfield_10162.value;
            const transicion = issueJSON.transition.transitionId;
            var esfuerzoTotal = issueJSON.issue.fields.customfield_10059;
            var diferencia = 0;
            //fileUtils.comentar('JSON WPF:' + JSON.stringify(issueJSON));
            var fileJSON = JSON.parse(fs.readFileSync('./utils_json/app.json', 'utf8'));
            aplicacion = fileJSON[aplicacion];
            //************************************** FUNCIONES ******************************/

            //*******************************************************************************/
            //fileUtils.comentar('Aplicacion:' + aplicacion);

            //############################################ MAIN ###################################################
            //@@@@@@@@@@@@@@@@@@@@@@@@   actualizar issue @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            var options = {
                method: 'GET',
                url: urlJira + '/rest/api/3/project/AP/properties/BL',
                headers: {
                    'Authorization': auth,
                    'Accept': 'application/json'
                }
            };
            request(options, async function (error, response, body) {
                if (error) throw new Error(error);
                console.log(
                    'Response: ' + response.statusCode + ' ' + response.statusMessage
                );
                json = JSON.parse(body);
                lista = json.value;
                console.log('aplicacion:' + aplicacion);
                valor = lista[aplicacion];
                switch (transicion) {
                    case 211: //aprobar estimación por negocio
                        if (esfuerzoTotal > valor[2]) console.log('NO HAY PRESUPUESTO')
                        else {
                            diferencia = valor[2] - esfuerzoTotal;
                            console.log('Diferencia:' + diferencia.toString());
                            b = await utils.editIssueBudget(urlJira, auth, issuekey, diferencia);
                            var Adiferencia = Array.from([diferencia.toString()]);
                            lista[aplicacion][2] = Adiferencia;
                            console.log('BL:' + JSON.stringify(json));
                            var options = {
                                method: 'PUT',
                                url: urlJira + '/rest/api/3/project/AP/properties/BL',
                                headers: {
                                    'Authorization': auth,
                                    'Accept': 'application/json'
                                },
                                body: JSON.stringify(lista)
                            };

                            request(options, function (error, response, body) {
                                if (error) throw new Error(error);
                                console.log(
                                    'Response: ' + response.statusCode + ' ' + response.statusMessage
                                );
                            });
                        }
                        break;
                    case 41:
                        if (esfuerzoTotal > valor[2]) console.log('NO HAY PRESUPUESTO')
                        else {
                            diferencia = valor[2] - esfuerzoTotal;
                            console.log('Diferencia:' + diferencia.toString());
                            b = await utils.editIssueBudget(urlJira, auth, issuekey, diferencia);
                            var Adiferencia = Array.from([diferencia.toString()]);
                            lista[aplicacion][2] = Adiferencia;
                            console.log('BL:' + JSON.stringify(json));
                            var options = {
                                method: 'PUT',
                                url: urlJira + '/rest/api/3/project/AP/properties/BL',
                                headers: {
                                    'Authorization': auth,
                                    'Accept': 'application/json'
                                },
                                body: JSON.stringify(lista)
                            };

                            request(options, function (error, response, body) {
                                if (error) throw new Error(error);
                                console.log(
                                    'Response: ' + response.statusCode + ' ' + response.statusMessage
                                );
                            });
                        }
                        break;
                    case 201: //aprobar requerimientos
                        fileUtils.comentar("/******** iBudget::Aprobar requerimientos  ==> " + valor[2]);
                        b = await utils.editIssueBudget(urlJira, auth, issuekey, valor[2]);
                        return '';
                    case 181: //iniciar (peticiones extraordinarias)
                        fileUtils.comentar("/******** iBudget::Iniciar peticion extraordinaria  ==> " + valor[2]);
                        b = await utils.editIssueBudget(urlJira, auth, issuekey, valor[2]);
                        return '';
                    default:
                        // code block
                        fileUtils.comentar("/******** iBudget::NO HAY TRANSICIÓN PARA BUDGET ==>");
                        return '';
                }
                res.sendStatus(200);
                fileUtils.comentar('FIN DE PROCESO');
            });
            //###################################################################################################

        } catch (error) {
            console.log("::ERROR app.post::" + error);
        }
    });
    /**
     * Route serving link to create in postfunction workflows the create remaining Budget option.
     * @name get/create-budget
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/create-budget', function (req, res) {
        res.render('workflow/create_budget', {

        });
    });
    /**
     * Route serving link to edit in postfunction workflows the create remaining Budget option.
     * @name get/edit-budget
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/edit-budget', function (req, res) {
        res.render('workflow/edit_budget', {

        });
    });
    /**
     * Route serving link to view in postfunction workflows the create remaining Budget option.
     * @name get/view-budget
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/view-budget', function (req, res) {
        res.render('workflow/view_budget', {

        });
    });
    /**
     * Route serving link to create BaseLine.
     * @name get/baseline
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.get('/baseline', addon.authenticate(), function (req, res) {
        let projectKey = req.query.projectKey;
        let budget = {};
        let lb = "";
        var options = {
            method: 'GET',
            url: urlJira + '/rest/api/3/project/' + projectKey + '/properties/BL',
            headers: {
                'Authorization': auth,
                'Accept': 'application/json'
            }
        };
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            console.log(
                'Response: ' + response.statusCode + ' ' + response.statusMessage
            );
            lb = JSON.parse(body);
            //budget = Object.entries(lb.value);
            fileUtils.comentar(budget);
            res.render('baseline/form', {
                title: 'Linea Base',
                projectKey: projectKey || '(Unknown project)',
                urlJira: urlJira,
                lb,
            });
        });
    });
    /**
     * Route serving link to update BaseLine.
     * @name post/update-baseline
     * @function
     * @memberof module:express/router~app
     * @inner
     * @param {string} path - Express path
     * @param {boolean} isAuthenticate -jwt token valid
     * @param {callback} middleware - Express middleware.
     */
    app.post('/update-baseline/:projectKey', addon.checkValidToken(), function (req, res) {
        BL.storeBaseline(req.params.projectKey, req.body.value, auth, urlJira);
        res.send(200);
    });
}